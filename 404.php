<?php

/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 */
get_header();
?>

<main>
	<section class="container">
		<h1 class="page-title">404</h1>
		<p class="page-message"><?php _e('We were unable to find the page you are looking for.', 'higimulher'); ?></p>
		<a class="back-home" href="<?php echo get_home_url(); ?>"><?php _e('Return to home page', 'higimulher'); ?></a>
	</section>
</main>

<?php
get_footer();
