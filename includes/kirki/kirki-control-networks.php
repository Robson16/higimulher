<?php

new \Kirki\Section(
	'section_social_networks',
	array(
		'title'       => esc_html__('Social Networks', 'higimulher'),
		'description' => esc_html__('Social networks pages.', 'higimulher'),
		'priority'    => 160,
	)
);

new \Kirki\Field\URL(
	array(
		'settings' 		=> 'setting_whatsapp',
		'label'    		=> esc_html__('Whatsapp', 'higimulher'),
		'description' 	=> esc_html__('Whatsapp API`s URL', 'higimulher'),
		'section'  		=> 'section_social_networks',
		'default'  		=> 'https://api.whatsapp.com/send?phone=5511988887777',
		'priority' 		=> 10,
	)
);

new \Kirki\Field\URL(
	array(
		'settings' 		=> 'setting_facebook',
		'label'    		=> esc_html__('Facebook', 'higimulher'),
		'description' 	=> esc_html__('Facebook page URL', 'higimulher'),
		'section'  		=> 'section_social_networks',
		'default'  		=> 'https://www.facebook.com/',
		'priority' 		=> 10,
	)
);

new \Kirki\Field\URL(
	array(
		'settings' 		=> 'setting_instagram',
		'label'    		=> esc_html__('Instagram', 'higimulher'),
		'description'   => esc_html__('Instagram Page URL', 'higimulher'),
		'section'  		=> 'section_social_networks',
		'default'  		=> 'https://www.instagram.com/',
		'priority' 		=> 10,
	)
);

new \Kirki\Field\URL(
	array(
		'settings' 		=> 'setting_twitter',
		'label'    		=> esc_html__('Twitter', 'higimulher'),
		'description'  	=> esc_html__('Twitter Page URL', 'higimulher'),
		'section'  		=> 'section_social_networks',
		'default'  		=> 'https://twitter.com/',
		'priority' 		=> 10,
	)
);

new \Kirki\Field\URL(
	array(
		'settings' 		=> 'setting_linkedin',
		'label'    		=> esc_html__('Linkedin', 'higimulher'),
		'description'  	=> esc_html__('Linkedin page URL', 'higimulher'),
		'section'  		=> 'section_social_networks',
		'default'  		=> 'https://www.linkedin.com/',
		'priority' 		=> 10,
	)
);

new \Kirki\Field\URL(
	array(
		'settings' 		=> 'setting_pinterest',
		'label'    		=> esc_html__('Pinterest', 'higimulher'),
		'description'  	=> esc_html__('Pinterest page URL', 'higimulher'),
		'section'  		=> 'section_social_networks',
		'default'  		=> 'https://pinterest.com/',
		'priority' 		=> 10,
	)
);

new \Kirki\Field\URL(
	array(
		'settings' 		=> 'setting_youtube',
		'label'    		=> esc_html__('YouTube', 'higimulher'),
		'description'  	=> esc_html__('YouTube page URL', 'higimulher'),
		'section'  		=> 'section_social_networks',
		'default'  		=> 'https://youtube.com/',
		'priority' 		=> 10,
	)
);
