<?php

/**

 */

$networks = "";

if (get_theme_mod('setting_facebook')) {
    $networks .= sprintf('<a class="facebook" href="%s" target="_black" rel="noopener" aria-label="Facebook">
        <i class="fab fa-facebook-f"></i>
    </a>', esc_url(get_theme_mod('setting_facebook')));
}
if (get_theme_mod('setting_twitter')) {
    $networks .= sprintf('<a class="twitter" href="%s" target="_black" rel="noopener" aria-label="Twitter">
        <i class="fab fa-twitter"></i>
    </a>', esc_url(get_theme_mod('setting_twitter')));
}
if (get_theme_mod('setting_instagram')) {
    $networks .= sprintf('<a class="instagram" href="%s" target="_black" rel="noopener" aria-label="Instagram">
        <i class="fab fa-instagram"></i>
    </a>', esc_url(get_theme_mod('setting_instagram')));
}
if (get_theme_mod('setting_linkedin')) {
    $networks .= sprintf('<a class="linkedin" href="%s" target="_black" rel="noopener" aria-label="Linkedin">
        <i class="fab fa-linkedin-in"></i>
    </a>', esc_url(get_theme_mod('setting_linkedin')));
}
if (get_theme_mod('setting_pinterest')) {
    $networks .= sprintf('<a class="pinterest" href="%s" target="_black" rel="noopener" aria-label="Pinterest">
        <i class="fab fa-pinterest"></i>
    </a>', esc_url(get_theme_mod('setting_pinterest')));
}
if (get_theme_mod('setting_youtube')) {
    $networks .= sprintf('<a class="youtube" href="%s" target="_black" rel="noopener" aria-label="YouTube">
        <i class="fab fa-youtube"></i>
    </a>', esc_url(get_theme_mod('setting_youtube')));
}

echo '<div class="social-networks">' . $networks .  '</div>';
